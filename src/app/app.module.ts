import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MtrPaginatorModule } from '@mortar/angular/components/atomic/paginator';
import { MtrSortModule } from '@mortar/angular/components/atomic/sort';
import { MtrTableModule } from '@mortar/angular/components/atomic/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RemoteDatasourceExampleComponent } from './remote-datasource-example/remote-datasource-example.component';

@NgModule({
  declarations: [
    AppComponent,
    RemoteDatasourceExampleComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MtrPaginatorModule,
    MtrSortModule,
    MtrTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
