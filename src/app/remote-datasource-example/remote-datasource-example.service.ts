import { Injectable } from '@angular/core';
import { MtrRemotePaginatedTableDataSource } from '@mortar/angular/components/atomic/table';
import { mapTo, of, timer } from 'rxjs';

export interface TableRow {
  id: string;
  pickupDate: string;
}

@Injectable({ providedIn: 'root' })
export class RemoteDatasourceExampleService {
  public remoteDataSource: MtrRemotePaginatedTableDataSource<any> = new MtrRemotePaginatedTableDataSource<any>({
    requester: ({ pageIndex, pageSize, sorts }) => {
      console.log('Sort Column:', sorts);
      return timer(500).pipe(mapTo({
        data: this.getExampleData(),
        totalItems: 4,
        pageIndex: pageIndex,
        pageSize: pageSize,
      }));
    },
    initialPageIndex: 0,
    initialPageSize: 10,
    initialSort: { active: 'pickupDate', direction: 'asc' },
  });

  private getExampleData(): TableRow[] {
    return [
      { id: '1', pickupDate: '2022-01-01' },
      { id: '2', pickupDate: '2022-01-02' },
      { id: '3', pickupDate: '2022-01-03' },
      { id: '4', pickupDate: '2022-01-04' },
    ];
  }
}
