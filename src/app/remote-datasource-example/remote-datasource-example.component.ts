import { Component } from '@angular/core';
import { RemoteDatasourceExampleService } from './remote-datasource-example.service';

@Component({
  selector: 'remote-datasource-example',
  templateUrl: 'remote-datasource-example.component.html',
})
export class RemoteDatasourceExampleComponent {
  public remoteDataSource = this.remoteDatasourceExampleService.remoteDataSource;

  public constructor(private remoteDatasourceExampleService: RemoteDatasourceExampleService) {}
}
